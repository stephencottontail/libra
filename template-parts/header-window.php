<?php
$background_image_url = '';

if ( get_theme_mod( 'always_show_header' ) ) {
	$background_image_url = get_header_image();
}

if ( libra_has_featured_posts() && is_front_page() ) :
	$count = 1;
	$featured_posts = libra_get_featured_posts();
	
	if ( empty( $background_image_url ) && has_post_thumbnail( $featured_posts[0]->ID ) ) {
		$background_image_url = get_the_post_thumbnail_url( $featured_posts[0]->ID );
	} else {
		$background_image_url = get_theme_file_uri( '/inc/images/model.jpg' );
	}
	?>

	<section class="featured-wrapper" style="<?php printf( 'background-image: url(%s);', esc_url( $background_image_url ) ); ?>">
		<?php
		foreach ( $featured_posts as $post ) {
			setup_postdata( $post );
			
			if ( 1 == $count ) : ?>
				<div class="big-feature">
					<?php get_template_part( 'template-parts/content', 'first' ); ?>
				</div><!-- .big-feature -->
			<?php else : ?>
				<div class="row-feature">
					<?php get_template_part( 'template-parts/content', 'featured' ); ?>
				</div>
			<?php
			endif;
			
			$count++;
		}
		
		wp_reset_postdata();
		?>
	</section><!-- .featured-wrapper -->
<?php else : ?>
	<div class="libra-window" style="<?php empty( $background_image_url ) ? null : printf( 'background-image: url(%s);', $background_image_url ); ?>">
		<?php libra_page_titles(); ?>
	</div>
<?php endif; ?>
