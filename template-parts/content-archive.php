<?php
/**
 * Template part for displaying posts on archive pages
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package Libra
 */
?>

<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
	<?php if ( has_post_thumbnail() ) : ?>
		<div class="entry-thumbnail">
			<a href="<?php echo esc_url( get_permalink() ); ?>" rel="bookmark"><?php the_post_thumbnail(); ?></a>
		</div><!-- .entry-thumbnail -->
	<?php endif; ?>
	
	<div class="entry-inner-wrapper">
		<div class="entry-inner">
			<header class="entry-header">
				<?php the_title( sprintf( '<h2 class="entry-title"><a href="%s" rel="bookmark">', esc_url( get_permalink() ) ), '</a></h2>' ); ?>
			</header><!-- .entry-header -->
		
			<div class="entry-content">
				<?php
				the_excerpt();
		
				wp_link_pages( array(
					'before'      => sprintf( '<div class="page-links">%s', esc_html__( 'Pages:', 'libra' ) ),
					'after'       => '</div>',
					'link_before' => '<span class="page-number">',
					'link_after'  => '</span>'
				) );
				?>
			</div><!-- .entry-content -->
			
			<?php if ( ! is_search() ) : ?>
				<footer class="archive-footer">
					<?php echo libra_posted_on(); ?>
				</footer><!-- .entry-footer -->
			<?php endif; ?>
		</div><!-- .entry-inner -->
	</div><!-- .entry-inner-wrapper -->
</article><!-- #post-## -->
