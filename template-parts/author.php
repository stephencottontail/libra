<?php
/**
 * Template part for displaying the author biography on single post views
 *
 * @package Libra
 */
?>

<div class="author-bio-wrapper">
	<div class="author-bio">
		<?php
		if ( get_avatar( $post ) ) {
			echo get_avatar( $post, 64 );
		}
		?>
		
		<h2 class="author-name"><?php echo esc_html( get_the_author() ); ?></h2>
		<p class="author-biography"><?php the_author_meta( 'description' ); ?></p>
		
		<?php
		printf( '<a class="author-link" href="%1$s">%2$s</a>',
			esc_url( get_author_posts_url( get_the_author_meta( 'ID' ) ) ),
			sprintf( esc_html__( 'See all posts by %s', 'libra' ), get_the_author_meta( 'display_name', $post->post_author ) )
		);
		?>
	</div><!-- .author-bio -->
</div><!-- .author-bio-wrapper -->