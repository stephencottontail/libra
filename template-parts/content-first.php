<?php
/**
 * Template part for displaying the first featured post
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package Libra
 */
?>

<article id="post-<?php the_ID(); ?>" <?php post_class( 'featured' ); ?>>
	<header class="entry-header">
		<?php libra_featured_header(); ?>
	</header><!-- .entry-header -->
	
	<div class="entry-content">
		<?php the_excerpt(); ?>
	</div><!-- .entry-content -->
</article>
