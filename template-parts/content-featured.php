<?php
/**
 * Template part for displaying other featured posts
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package Libra
 */
?>

<article id="post-<?php the_ID(); ?>" <?php post_class( 'featured' ); ?>>
	<header class="entry-header">
		<?php libra_featured_header(); ?>
	</header><!-- .entry-header -->
</article>
