<?php
/**
 * The template for displaying all single posts
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/#single-post
 *
 * @package Libra
 */

get_header(); ?>

<main id="primary" class="content-area" role="main">
	<div class="content-wrapper">
		<?php
		while ( have_posts() ) : the_post();
	
			get_template_part( 'template-parts/content', get_post_format() );
	
			if ( ! is_attachment() ) {
				get_template_part( 'template-parts/author' );
			}
			
			the_post_navigation( array(
				'prev_text' => sprintf( '<span class="nav-title">%1$s</span>%%title', esc_html__( 'Previous Post', 'libra' ) ),
				'next_text' => sprintf( '<span class="nav-title">%1$s</span>%%title', esc_html__( 'Next Post', 'libra' ) )
			) );
	
			// If comments are open or we have at least one comment, load up the comment template.
			if ( comments_open() || get_comments_number() ) :
				comments_template();
			endif;
	
		endwhile; // End of the loop.
		?>
	</div><!-- .content-wrapper -->
</main><!-- #primary -->

<?php
get_sidebar();
get_footer();
