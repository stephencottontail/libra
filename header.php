<?php
/**
 * The header for our theme
 *
 * This is the template that displays all of the <head> section and everything up until <div id="content">
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package Libra
 */

?><!DOCTYPE html>
<html <?php language_attributes(); ?>>
<head>
<meta charset="<?php bloginfo( 'charset' ); ?>">
<meta name="viewport" content="width=device-width, initial-scale=1">
<link rel="profile" href="http://gmpg.org/xfn/11">

<?php wp_head(); ?>
</head>

<body <?php body_class(); ?>>
<div id="page" class="site">
	<a class="skip-link screen-reader-text" href="#content"><?php esc_html_e( 'Skip to content', 'libra' ); ?></a>

	<header id="masthead" class="site-header" role="banner">
		<?php if ( has_nav_menu( 'menu-1' ) ) : ?>
			<button class="panel-toggle" aria-expanded="false"><span class="icon"></span><span class="screen-reader-text"><?php esc_html_e( 'Show panel', 'libra' ); ?></span></button>
		<?php endif; ?>
		
		<div class="site-branding">
			<?php if ( is_front_page() && is_home() ) : ?>
				<h1 class="site-title"><a href="<?php echo esc_url( home_url( '/' ) ); ?>" rel="home"><?php bloginfo( 'name' ); ?></a></h1>
			<?php else : ?>
				<p class="site-title"><a href="<?php echo esc_url( home_url( '/' ) ); ?>" rel="home"><?php bloginfo( 'name' ); ?></a></p>
			<?php
			endif;

			$description = get_bloginfo( 'description', 'display' );
			if ( $description || is_customize_preview() ) : ?>
				<p class="site-description"><?php echo $description; /* WPCS: xss ok. */ ?></p>
			<?php endif; ?>
		</div><!-- .site-branding -->
	</header><!-- #masthead -->
	
	<?php if ( has_nav_menu( 'menu-1' ) ) : ?>
		<div class="panel closed">
			<div class="search-wrapper">
				<i class="genericon genericon-search"></i>
				<?php get_search_form(); ?>
			</div>
			
			<?php if ( has_nav_menu( 'menu-1' ) ) : ?>
				<nav id="primary-menu" class="site-navigation" role="navigation">
					<?php wp_nav_menu( array( 'theme_location' => 'menu-1', 'container' => false ) ); ?>
				</nav><!-- #primary-menu -->
			<?php endif; ?>
		</div><!-- .panel -->
	<?php
	endif;
	
	get_template_part( 'template-parts/header', 'window' );
	?>
	
	<div id="content" class="site-content">
