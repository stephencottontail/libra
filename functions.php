<?php
/**
 * Libra functions and definitions
 *
 * @link https://developer.wordpress.org/themes/basics/theme-functions/
 *
 * @package Libra
 */

if ( ! function_exists( 'libra_setup' ) ) :
/**
 * Sets up theme defaults and registers support for various WordPress features.
 *
 * Note that this function is hooked into the after_setup_theme hook, which
 * runs before the init hook. The init hook is too late for some features, such
 * as indicating support for post thumbnails.
 */
function libra_setup() {
	/*
	 * Make theme available for translation.
	 * Translations can be filed in the /languages/ directory.
	 * If you're building a theme based on Libra, use a find and replace
	 * to change 'libra' to the name of your theme in all the template files.
	 */
	load_theme_textdomain( 'libra', get_template_directory() . '/languages' );

	// Add default posts and comments RSS feed links to head.
	add_theme_support( 'automatic-feed-links' );

	/*
	 * Let WordPress manage the document title.
	 * By adding theme support, we declare that this theme does not use a
	 * hard-coded <title> tag in the document head, and expect WordPress to
	 * provide it for us.
	 */
	add_theme_support( 'title-tag' );

	/*
	 * Enable support for Post Thumbnails on posts and pages.
	 *
	 * @link https://developer.wordpress.org/themes/functionality/featured-images-post-thumbnails/
	 */
	add_theme_support( 'post-thumbnails' );

	// This theme uses wp_nav_menu() in one location.
	register_nav_menus( array(
		'menu-1' => esc_html__( 'Primary', 'libra' ),
	) );

	/*
	 * Switch default core markup for search form, comment form, and comments
	 * to output valid HTML5.
	 */
	add_theme_support( 'html5', array(
		'search-form',
		'comment-form',
		'comment-list',
		'gallery',
		'caption',
	) );
	
	// Add theme support for selective refresh for widgets.
	add_theme_support( 'customize-selective-refresh-widgets' );
	
	// Add support for Jetpack features
	add_theme_support( 'featured-content', array(
    'filter'     => 'libra_get_featured_posts',
    'max_posts'  => 3
	) );
	
	add_theme_support( 'jetpack-social-menu' );
	
	/*
	 * Style the TinyMCE editor
	 */
	add_editor_style( array( 'inc/css/editor-style.css', libra_fonts_url() ) );
	
	/*
	 * Add support for custom header
	 */
	add_theme_support( 'custom-header', apply_filters( 'libra_custom_header_args', array(
		'default-image'          => '',
		'default-text-color'     => '000000',
		'width'                  => 1000,
		'height'                 => 250,
		'flex-height'            => true,
	) ) );
}
endif;
add_action( 'after_setup_theme', 'libra_setup' );

/**
 * Functions to support Jetpack's featured content
 */
function libra_get_featured_posts() {
	return apply_filters( 'libra_get_featured_posts', array() );
}

function libra_has_featured_posts( $minimum = 1 ) {
	if ( is_paged() ) {
		return false;
	}
	
	$minimum = absint( $minimum );
	$featured_posts = apply_filters( 'libra_get_featured_posts', array() );
	
	if ( ! is_array( $featured_posts ) ) {
		return false;
	}
 
	if ( $minimum > count( $featured_posts ) ) {
		return false;
	}
   
	return true;
}

function libra_set_body_classes( $classes ) {
	if ( libra_has_featured_posts() ) {
		$classes[] .= 'has-featured-content';
	}
	
	return $classes;
}
add_filter( 'body_class', 'libra_set_body_classes' );

/**
 * Set the content width in pixels, based on the theme's design and stylesheet.
 *
 * Priority 0 to make it available to lower priority callbacks.
 *
 * @global int $content_width
 */
function libra_content_width() {
	$GLOBALS['content_width'] = apply_filters( 'libra_content_width', 940 );
}
add_action( 'after_setup_theme', 'libra_content_width', 0 );

/**
 * Register widget area.
 *
 * @link https://developer.wordpress.org/themes/functionality/sidebars/#registering-a-sidebar
 */
function libra_widgets_init() {
	register_sidebar( array(
		'name'          => esc_html__( 'Sidebar', 'libra' ),
		'id'            => 'sidebar-1',
		'description'   => esc_html__( 'Add widgets here.', 'libra' ),
		'before_widget' => '<section id="%1$s" class="widget %2$s">',
		'after_widget'  => '</section>',
		'before_title'  => '<h2 class="widget-title">',
		'after_title'   => '</h2>',
	) );
}
add_action( 'widgets_init', 'libra_widgets_init' );

/**
 * Enqueue scripts and styles.
 */
function libra_scripts() {
	wp_enqueue_style( 'genericons', get_theme_file_uri( '/inc/genericons/genericons/genericons.css' ) );
	wp_enqueue_style( 'libra-google-fonts', libra_fonts_url() );
	wp_enqueue_style( 'libra-style', get_stylesheet_uri(), array( 'libra-google-fonts', 'genericons' ) );

	wp_enqueue_script( 'libra-navigation', get_theme_file_uri( '/js/libra.js' ), array( 'jquery' ), '20151215', true );
	wp_localize_script( 'libra-navigation', 'libraChildMenuToggle', array(
		'expand'   => __( 'expand child menu', 'libra' ),
		'collapse' => __( 'collapse child menu', 'libra' )
	) );
	wp_enqueue_script( 'libra-skip-link-focus-fix', get_theme_file_uri( '/js/skip-link-focus-fix.js' ), array(), '20151215', true );

	if ( is_singular() && comments_open() && get_option( 'thread_comments' ) ) {
		wp_enqueue_script( 'comment-reply' );
	}
}
add_action( 'wp_enqueue_scripts', 'libra_scripts' );

/**
 * Load Google fonts, and also allow users to disable them if needed
 */
function libra_fonts_url() {
	$fonts_url = '';

	/**
	 * Translators: If there are characters in your language that are not
	 * supported by Poppins, translate this to 'off'. Do not translate
	 * into your own language.
	 */
	if ( 'off' !== _x( 'on', 'Montserrat font: on or off', 'libra' ) ) {
		$fonts[] = 'Montserrat:400,400i,700,700i';
	}
	
	/**
	 * Translators: If there are characters in your language that are not
	 * supported by Playfair Display, translate this to 'off'. Do not translate
	 * into your own language.
	 */
	if ( 'off' !== _x( 'on', 'Playfair Display font: on or off', 'libra' ) ) {
		$fonts[] = 'Playfair Display';
	}

	/**
	 * Translators: If there are characters in your language that are not
	 * supported by Oxygen Mono, translate this to 'off'. Do not translate
	 * into your own language.
	 */
	if ( 'off' !== _x( 'on', 'Oxygen Mono font: on or off', 'libra' ) ) {
		$fonts[] = 'Oxygen Mono';
	}
	
	if ( $fonts ) {
		$query_args = array(
			'family' => urlencode( implode( '|', $fonts ) )
		);
		
		$fonts_url = add_query_arg( $query_args, 'https://fonts.googleapis.com/css' );
	}
	
	return esc_url_raw( $fonts_url );
}

/**
 * We're using excerpts, so let's make them more interesting
 */
function libra_excerpt_length() {
	return 15;
}
add_filter( 'excerpt_length', 'libra_excerpt_length' );

function libra_excerpt_more() {
	global $post;
	
	return sprintf( '&hellip;<div class="read-more"><a class="read-more-link" href="%1$s" rel="bookmark">%2$s <span class="screen-reader-text">%3$s</span></a></div>',
		esc_url( get_the_permalink( $post ) ),
		esc_html__( 'Continue Reading', 'libra' ),
		get_the_title()
	);
}
add_filter( 'excerpt_more', 'libra_excerpt_more' );

/**
 * Return the appropriate header image depending on what type of page we're viewing
 */
function libra_set_background_image() {
	global $post;
	global $posts;
	$background_image_url = '';
	
	if ( is_home() || is_archive() || ( is_search() && $posts ) ) {
		if ( has_post_thumbnail( $posts[0]->ID ) ) {
			$background_image_url = wp_get_attachment_url( get_post_thumbnail_id( $posts[0]->ID ) );
		}
	} 
	
	if ( is_single() && ! post_password_required() ) {
		$background_image_url = wp_get_attachment_url( get_post_thumbnail_id() );
	}
	
	if ( empty( $background_image_url ) ) {
		$background_image_url = get_theme_file_uri( '/inc/images/model.jpg' );
	}

	$custom_css = sprintf( '.libra-window { background-image: url(%s);', $background_image_url ); 
	wp_add_inline_style( 'libra-style', $custom_css );
}
add_action( 'wp_enqueue_scripts', 'libra_set_background_image', 15 );

/**
 * Remove <p> tags from get_the_archive_description
 */
function libra_custom_archive_description( $description ) {
	$remove = array( '<p>', '</p>' );
	$description = str_replace( $remove, '', $description );

	return $description;
}
add_filter( 'get_the_archive_description', 'libra_custom_archive_description' );

/**
 * Custom template tags for this theme.
 */
require get_theme_file_path( '/inc/template-tags.php' );

/**
 * Custom functions that act independently of the theme templates.
 */
require get_theme_file_path( '/inc/extras.php' );

/**
 * Customizer additions.
 */
require get_theme_file_path( '/inc/customizer.php' );

/**
 * Load Jetpack compatibility file.
 */
require get_theme_file_path( '/inc/jetpack.php' );
