<?php
/**
 * The template for displaying archive pages
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package Libra
 */

get_header(); ?>

<main id="primary" class="content-area" role="main">
	<div class="content-wrapper archive-row">
		
		<?php
		if ( have_posts() ) :
		
			/* Start the Loop */
			while ( have_posts() ) : the_post();
			
				get_template_part( 'template-parts/content', 'archive' );
	
			endwhile;
	
			the_posts_pagination( array(
				'before_page_number' => sprintf( '<span class="screen-reader-text">%s </span>', esc_html_x( 'Page', 'used before page numbers in pagination', 'libra' ) )
			) );
	
		else :
	
			get_template_part( 'template-parts/content', 'none' );
	
		endif;
		?>
		
	</div><!-- .content-wrapper -->
</main><!-- #main -->

<?php
get_sidebar();
get_footer();
