# Libra

Libra is a contemporary magazine or portfolio theme that features clean typography and a grid to easily display articles, images, or just blog posts.

## Callouts

If you set a manual excerpt for your posts, Libra will display it in a larger font on the single post view, drawing your readers' eyes to the most important parts of your posts.

## Pull Quotes

Libra allows you to set block quotes off to the left or the right, allowing you to draw your readers' eyes to important quotes. To use this feature, switch to the Text view (if you're not using it already) and add the class `pull-left` or `pull-right` to the `<blockquote>` tag: `<blockquote class="pull-left"></blockquote>` or `<blockquote class="pull-right"></blockquote>`.

## Jetpack Features

Libra makes use of some of [Jetpack](http://jetpack.me/) for some of its features. A WordPress.com account is not required for any of Libra's functionality.

### Social Media Menu

Libra features a special menu with links to your social media sites in the footer, with icons for the major social media networks. To use this menu, follow these steps:

1. Go to your Dashboard and then go to Appearance > Menus. Create a new menu and name it whatever you'd like.

2. Select "Custom Links" from the accordion menu on the left (where it says "Pages", "Custom Links", and "Categories"), and in the text boxes marked "URL", enter your URL. For example, for Twitter, you would write https://twitter.com/your-twitter-username.

3. Click the "Add to Menu" button. Repeat for every social media network you'd like to add.

4. Once your menu is created, click the "Save Menu" button, and then click on the "Manage Locations" tab near the top of the screen. Assign your newly-created menu to the "Social Menu" location.

### Featured Content

Libra can display up to three featured posts on the front page. By default, Libra will feature the first three posts with the tag "featured", but you can easily change this by going to Dashboard > Appearance > Customize > Featured Content > Tag name, and typing in the new tag.

## Changelog 

* Version 1.1.0, May 31
	* Deleted some unused files
	* Implemented selective refresh for the footer text
	* Minor bug fixes
	
* Version 1.0.0, April 26
	* First submitted to the WP.org repository
	
## Credits

* Libra itself is based on [Underscores](http://underscores.me), which is copyright Automattic, Inc. Both Libra and Underscores are licensed under the [GPL, v2](https://www.gnu.org/licenses/old-licenses/gpl-2.0.en.html)
* The bundled image, which was obtained from [Unsplash](https://unsplash.com/photos/UNOl_D9T9Ls), is copyright Christopher Campbell, and licensed under the [CCO](https://creativecommons.org/publicdomain/zero/1.0/)
* [Montserrat](https://fonts.google.com/specimen/Montserrat) font is copyright Julieta Ulanovsky, [Playfair Display](https://fonts.google.com/specimen/Playfair+Display) font is copyright Claus Eggers Sorensen, and [Oxygen Mono](https://fonts.google.com/specimen/Oxygen+Mono) font is copyright Vernon Adams. All three fonts are licensed under the [SIL Open Font License](http://scripts.sil.org/cms/scripts/page.php?site_id=nrsi&id=OFL_web)
* [Genericons](https://github.com/Automattic/Genericons) is copyright Automattic and licensed under the [GPL v2](https://www.gnu.org/licenses/old-licenses/gpl-2.0.en.html)