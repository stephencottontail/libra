<?php
/**
 * Custom template tags for this theme
 *
 * Eventually, some of the functionality here could be replaced by core features.
 *
 * @package Libra
 */

if ( ! function_exists( 'libra_featured_header' ) ) :
/**
 * Displays the header for featured posts
 */
function libra_featured_header() {
	$first_category = wp_get_object_terms( get_the_ID(), 'category', array( 
		'orderby' => 'name',
		'order'   => 'ASC',
		'number'  => 1
	) );
	
	$first_category_list = esc_url( get_category_link( $first_category[0] ) );
	
	printf( '<a class="featured-cat-link" href="%1$s">%2$s</a><h2 class="entry-title"><a href="%3$s" rel="bookmark">%4$s</a></h2>',
		$first_category_list,
		$first_category[0]->name,
		esc_url( get_the_permalink() ),
		esc_html( get_the_title() )
	);
}
endif;

if ( ! function_exists( 'libra_page_titles' ) ) :
/**
 * Displays an appropriate page title depending on what kind of page we're viewing
 */
function libra_page_titles() {
	global $wp_query;
	global $post;
	
	$title = '';
	$subtitle = '';
	
	if ( is_singular() ) {
		$title = get_the_title();
		
		if ( ! is_page() ) {
			$subtitle = libra_posted_on();
		}
	}
	
	if ( is_archive() ) {
		$title = get_the_archive_title();
		$subtitle = get_the_archive_description();
	}
	
	if ( is_search() ) {
		$title = sprintf( esc_html__( 'Searched for &ldquo;%s&rdquo;', 'libra' ), get_search_query() );
		$subtitle = sprintf( esc_html( _nx( '1 Result', '%s Results', $wp_query->found_posts, 'Number of search results', 'libra' ) ), $wp_query->found_posts );
	}
	
	if ( is_404() ) {
		$title = esc_html__( 'Oops!', 'libra' );
	}
	
	if ( $title || $subtitle ) {
		printf( '<div class="title-box"><h1 class="page-title">%1$s</h1><p class="page-subtitle">%2$s</p></div>', $title, $subtitle );
	}
}
endif;

if ( ! function_exists( 'libra_posted_on' ) ) :
/**
 * Prints HTML with meta information for the current post-date/time and author.
 */
function libra_posted_on() {
	global $post;
		
	$posted_on = sprintf( '<time class="entry-date" datetime="%1$s"><a href="%2$s" rel="bookmark">%3$s</time></a>',
		esc_attr( get_the_date( 'c' ) ),
		esc_url( get_permalink() ),
		esc_html( get_the_date() )
	);

	$byline = sprintf( esc_html_x( 'by %s', 'post author', 'libra' ),
		esc_html( get_the_author_meta( 'user_nicename', $post->post_author ) )
	);

	$entry_meta = $posted_on . '<i class="genericon genericon-dot"></i>' . $byline;
	 
	if ( is_single() && ! post_password_required() && ( comments_open() || get_comments_number() ) ) {
		$number = get_comments_number();
		$title = get_the_title();
		
		if ( '0' == get_comments_number() ) {
			/* Translators: %s = post title */
			$comment_link_text = sprintf( __( 'No Comments <span class="screen-reader-text"> on %s</span>', 'libra' ), $title );
		}
		
		if ( '1' == get_comments_number() ) {
			/* Translators: %s = post title */
			$comment_link_text = sprintf( __( '1 Comment <span class="screen-reader-text"> on %s</span>', 'libra'), $title );
		}
		
		if ( '1' < get_comments_number() ) {
			/* Translators: %s = post title */
			$comment_link_text = _n( '%1$s Comment<span class="screen-reader-text"> on %2$s</span>', '%1$s Comments<span class="screen-reader-text"> on %2$s</span>', get_comments_number(), 'libra' );
			$comment_link_text = sprintf( $comment_link_text, number_format_i18n( $number ), $title );
		}
		
		$comments_link = sprintf( '<a href="%1$s">%2$s</a>',
			get_comments_link(),
			$comment_link_text
		);
		
		$entry_meta .= '<i class="genericon genericon-dot"></i>' . $comments_link;
	}
	
	return $entry_meta;
}
endif;

if ( ! function_exists( 'libra_entry_footer' ) ) :
/**
 * Prints HTML with meta information for the categories, tags and comments.
 */
function libra_entry_footer() {
	// Hide category and tag text for pages.
	if ( 'post' === get_post_type() ) {
		/* translators: used between list items, there is a space after the comma */
		$categories_list = get_the_category_list( esc_html__( ', ', 'libra' ) );
		if ( $categories_list && libra_categorized_blog() ) {
			printf( '<span class="cat-links">%s</span>', $categories_list ); // WPCS: XSS OK.
		}

		/* translators: used between list items, there is a space after the comma */
		$tags_list = get_the_tag_list( '', esc_html__( ', ', 'libra' ) );
		if ( $tags_list ) {
			printf( '<span class="tags-links">%s</span>', $tags_list ); // WPCS: XSS OK.
		}
	}

	edit_post_link(
		sprintf(
			/* translators: %s: Name of current post */
			esc_html__( 'Edit %s', 'libra' ),
			the_title( '<span class="screen-reader-text">', '</span>', false )
		),
		'<span class="edit-link">',
		'</span>'
	);
}
endif;

if ( ! function_exists( 'libra_show_comments' ) ) :
/**
 * Custom function to display comments
 */
function libra_show_comments( $comment, $args, $depth ) {
	$GLOBALS['comment'] = $comment;
	global $post;
  
	if ( 'pingback' == $comment->comment_type || 'trackback' == $comment->comment_type ) : ?>
		<li id="div-comment-<?php comment_ID(); ?>" <?php comment_class(); ?>>
			<article class="pingback-body">
				<div class="pingback-content">
					<?php
					/* translators: There is a space afterwards */
					esc_html_e( 'Pingback: ', 'libra' );
					?>
					<cite><?php comment_author_link(); ?></cite>
					<?php edit_comment_link( esc_html__( 'Edit', 'libra' ), '<span class="pingback-edit">', '</span>' ); ?>
				</div>
			</article>
	
	<?php else : ?>
		<li <?php comment_class( empty( $args['has_children'] ) ? '' : 'parent' ); ?>>
			<article class="comment-body">
				<div class="comment-content">
					<?php
					if ( '0' != $args['avatar_size'] ) :
						echo get_avatar( $comment, $args['avatar_size'] );
					endif;
          
					if ( ! empty( $comment->comment_parent ) ) :
						$parent_comment = get_comment( $comment->comment_parent );
						
						$reply_to = sprintf( '<a href="%1$s">%2$s</a>',
							esc_url( get_comment_link( $parent_comment ) ),
							esc_html( $parent_comment->comment_author )
						);
						?>
						<span class="in-reply-to"><?php printf( esc_html__( 'In reply to %s', 'libra' ), $reply_to ); ?></span>
					<?php
					endif;
					
					if ( '0' == $comment->comment_approved ) : ?>
						<span class="awaiting-moderation"><?php esc_html_e( 'Your comment is awaiting moderation.', 'libra' ); ?></span>
					<?php
					endif;
					
					comment_text();
					?>
					<div class="comment-actions">
						<?php
						comment_reply_link( array_merge( $args, array(
							'add_below' => 'comment',
							'depth'     => $depth,
							'max_depth' => $args['max_depth'],
							'before'    => '<span class="comment-reply">',
							'after'     => '</span>',
						) ) );
						
						edit_post_link( esc_html__( 'Edit', 'libra' ), '<span class="comment-edit">', '</span>' );
            ?>
					</div><!-- .comment-actions -->
				</div><!-- .comment-content -->
				
				<header class="comment-header">
					<cite class="fn"><?php echo get_comment_author_link(); ?></cite>
					<a href="<?php echo esc_url( get_comment_link( $comment->comment_ID ) ); ?>" class="comment-time"><time datetime="<?php comment_time( 'c' ); ?>"><?php printf( esc_html_x( '%1$s, %2$s', '1: date, 2: time', 'libra' ),
						get_comment_date(), /* %1$s */
						get_comment_time() /* %2$s */
					); ?></time></a>
				</header><!-- .comment-header -->
			</article>
			
	<?php
	endif; // end check for comment type
	
} // end of function
endif; // end of check for function's existence

/**
 * Returns true if a blog has more than 1 category.
 *
 * @return bool
 */
function libra_categorized_blog() {
	if ( false === ( $all_the_cool_cats = get_transient( 'libra_categories' ) ) ) {
		// Create an array of all the categories that are attached to posts.
		$all_the_cool_cats = get_categories( array(
			'fields'     => 'ids',
			'hide_empty' => 1,
			// We only need to know if there is more than one category.
			'number'     => 2,
		) );

		// Count the number of categories that are attached to the posts.
		$all_the_cool_cats = count( $all_the_cool_cats );

		set_transient( 'libra_categories', $all_the_cool_cats );
	}

	if ( $all_the_cool_cats > 1 ) {
		// This blog has more than 1 category so libra_categorized_blog should return true.
		return true;
	} else {
		// This blog has only 1 category so libra_categorized_blog should return false.
		return false;
	}
}

/**
 * Flush out the transients used in libra_categorized_blog.
 */
function libra_category_transient_flusher() {
	if ( defined( 'DOING_AUTOSAVE' ) && DOING_AUTOSAVE ) {
		return;
	}
	// Like, beat it. Dig?
	delete_transient( 'libra_categories' );
}
add_action( 'edit_category', 'libra_category_transient_flusher' );
add_action( 'save_post',     'libra_category_transient_flusher' );
