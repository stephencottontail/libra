<?php
/**
 * Libra Theme Customizer
 *
 * @package Libra
 */

/**
 * Add postMessage support for site title and description for the Theme Customizer.
 *
 * @param WP_Customize_Manager $wp_customize Theme Customizer object.
 */
function libra_customize_register( $wp_customize ) {
	$wp_customize->get_setting( 'blogname' )->transport         = 'postMessage';
	$wp_customize->get_setting( 'blogdescription' )->transport  = 'postMessage';
	$wp_customize->get_setting( 'header_textcolor' )->transport = 'postMessage';
	
	$wp_customize->add_section( 'footer_text', array(
		'title'       => esc_html__( 'Footer Text', 'libra' ),
		'description' => esc_html__( 'Text entered in this form will appear in place of the default footer text. A limited amount of HTML may be used.', 'libra' ),
		'priority'    => 35
	) );
  
  $wp_customize->add_setting( 'footer_text', array(
		'default'           => null,
		'type'              => 'theme_mod',
		'capability'        => 'edit_theme_options',
		'transport'         => 'postMessage',
		'sanitize_callback' => 'libra_sanitize_footer'
	) );
  
	$wp_customize->add_control( 'footer_text', array(
		'label'    => esc_html__( 'Footer Text', 'libra' ),
		'section'  => 'footer_text',
		'settings' => 'footer_text',
		'type'     => 'text'
	) );
  
  $wp_customize->selective_refresh->add_partial( 'footer_text', array(
	  'selector'        => '.site-info',
	  'render_callback' => 'libra_customize_partial_footer_text'
  ) );
  
	$wp_customize->add_setting( 'always_show_header', array(
		'default'           => false,
		'type'              => 'theme_mod',
		'capability'        => 'edit_theme_options',
		'sanitize_callback' => 'libra_sanitize_checkbox'
	) );
  
	$wp_customize->add_control( 'always_show_header', array(
		'label'       => esc_html__( 'Always Display Header Image', 'libra' ),
		'description' => esc_html__( 'Select this option to always display the chosen image, even on the home page or when viewing an individual post', 'libra' ),
		'section'     => 'header_image',
		'settings'    => 'always_show_header',
		'type'        => 'checkbox'
	) );
}
add_action( 'customize_register', 'libra_customize_register' );

function libra_sanitize_footer( $value ) {
	if ( $value ) {
		return wp_kses( $value,
			array( 'a' => array( 'href' => array() ),
				'strong' => array(),
				'em'     => array(),
				'span'   => array( 'class' => array() )
			)
		);
	} else {
		return false;
	}
}

function libra_sanitize_checkbox( $checked ) {
	return ( ( isset( $checked ) && true == $checked ) ? true : false );
}

function libra_customize_partial_footer_text() {
	$footer_text = get_theme_mod( 'footer_text' );
	
	if ( $footer_text ) {
		return wp_kses( $footer_text,
			array( 'a' => array( 'href' => array() ),
				'strong' => array(),
				'em'     => array(),
				'span'   => array( 'class' => array() )
			)
		);
	}
}

/**
 * Binds JS handlers to make Theme Customizer preview reload changes asynchronously.
 */
function libra_customize_preview_js() {
	wp_enqueue_script( 'libra_customizer', get_theme_file_uri( '/js/customizer.js' ), array( 'customize-preview' ), '20151215', true );
}
add_action( 'customize_preview_init', 'libra_customize_preview_js' );
